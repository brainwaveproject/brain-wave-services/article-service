## Create Brain Wave article Database

1. Launch `1_create_schema.sql` script to create schema.

2. Launch `2_create_article_table.sql` script to create article table.