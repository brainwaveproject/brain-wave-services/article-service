package com.brainwave.articleservice.controller;

import com.brainwave.articleservice.domain.Article;
import com.brainwave.articleservice.service.ArticleService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1.0/articles")
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ArticleController {

    ArticleService articleService;

    @GetMapping
    public List<Article> getAll() {

        return articleService.findAll();
    }
}
