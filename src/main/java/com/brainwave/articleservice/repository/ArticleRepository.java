package com.brainwave.articleservice.repository;

import com.brainwave.articleservice.domain.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author florian935
 */
@Repository
public interface ArticleRepository extends JpaRepository<Article, String> {
}
