package com.brainwave.articleservice.security.jwt;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@Component
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class JwtRequestFilter extends OncePerRequestFilter {

    JwtTokenUtil jwtTokenUtil;
    String AUTHORIZATION = "Authorization";
    String BEARER = "Bearer ";
    int BEARER_LENGHT = 7;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {

        try {
            authenticateFromJwt(request);
        } catch (Exception e) {
            logError(e);
        }

        chain.doFilter(request, response);
    }

    private void authenticateFromJwt(HttpServletRequest request) {

        final String jwt = parseJwt(request);

        if (StringUtils.hasText(jwt) && jwtTokenUtil.validateJwtToken(jwt)) {
            setSecurityAuthentication(jwt);
        }
    }

    private void setSecurityAuthentication(String jwt) {

        final String username = jwtTokenUtil.getUsernameFromJwtToken(jwt);

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(
                        username, null, AuthorityUtils.NO_AUTHORITIES);

        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    private void logError(Exception e) {

        log.error(e.getMessage());
    }

    private String parseJwt(HttpServletRequest request) {

        String headerAuth = request.getHeader(AUTHORIZATION);

        return StringUtils.hasText(headerAuth) && headerAuth.startsWith(BEARER)
                ? headerAuth.substring(BEARER_LENGHT)
                : null;
    }
}
