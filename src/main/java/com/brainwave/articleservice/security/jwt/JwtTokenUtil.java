package com.brainwave.articleservice.security.jwt;

import io.jsonwebtoken.Jwts;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@Component
@Slf4j
@FieldDefaults(level = PRIVATE)
public class JwtTokenUtil {

    @Value("${jwt.secret}")
    private String secret;

    public String getUsernameFromJwtToken(String token) {

        return Jwts
                .parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    public boolean validateJwtToken(String authToken) {

        try {
            parseToken(authToken);
            return true;
        } catch (Exception e) {
            logError(e);
            return false;
        }

    }

    private void parseToken(String authToken) {

        Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken);
    }

    private void logError(Exception exception) {

        log.error("Invalid JWT: {}", exception.getMessage());
    }
}
