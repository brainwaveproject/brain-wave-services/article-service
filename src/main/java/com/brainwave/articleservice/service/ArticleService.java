package com.brainwave.articleservice.service;

import com.brainwave.articleservice.domain.Article;

import java.util.List;

/**
 * @author florian935
 */
public interface ArticleService {
    List<Article> findAll();
}
