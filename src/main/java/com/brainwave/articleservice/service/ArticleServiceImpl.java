package com.brainwave.articleservice.service;

import com.brainwave.articleservice.domain.Article;
import com.brainwave.articleservice.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ArticleServiceImpl implements ArticleService {
    ArticleRepository articleRepository;

    @Override
    public List<Article> findAll() {

        return articleRepository.findAll();
    }
}
